# BI_ARD Sem

Zadání: 

1. jednoduchá digitální hra
Vymyslete digitální hru, která bude běžet celá na Esploře (například tetris), 
bude využívat display a ovládací prvky.

autor zadání: Ivo Háleček

Přesnějí: Brick Game X in 1 - Závody
https://retroconsoles.fandom.com/wiki/Brick_Game

Specifikace:

Klasické retro závody z herní konzole  Brick Game X in 1. Hráč ovládá formuli
a snaží se předjíždet ostatní závodníky. 
    Za každého předjetého dostává bod. Hráč ale musí dát pozor aby do 
nikoho nenarazil. Musí proto přejíždět z levého do pravého pruhu tak aby 
se vyhnul pomaleji jedoucím závodníkům. Čím dále se závodník dostane tím 
rychleji se formule budou pohybovat. Hra končí ve chvlí kdy dojde ke 
kolizi hráče s jíným závodníkem.

Návod:

Proto aby jste si mohli tuto hru zahrát budete potřebovat Arduino Esplora.
Arduino připojte k počítači a nahrajte main.io.
Hra by se měla ihned spustit.
Ovládání:
    SWITCH_DOWN: mění pozici hráče
    SWITCH_UP: resetuje hru
Snažte se vyhýbat protijedoucím autům. Za každý ujety metr dostanete score.
Čím více autum se vyhnete tím rychleji dostáváte score ale také pojedete
rychleji. Na dispeji mužete vidět aktuální rychlost, score a nejvyšší dosažené 
score.

Good luck and godspeed.