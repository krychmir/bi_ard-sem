/**
* @file main.ino
* @author Miroslav Krýcha
*
* @brief Klasické retro závody z herní konzole  Brick Game X in 1.
*
*/

#include <Esplora.h>
#include <stdio.h>
#include <stdlib.h>
#include <SPI.h>
#include <TFT.h>
#include <EEPROM.h>

/*
Rozvržení rozhraní

Display 160x128px

Herní plocha
  4X, 4Y, 80x120px

  Jeden Block 5x5
  Mezi bloky 1px mezera

  Počet Bloků na délku: 19

  Kolize proběhne pokud se formule vykreslí na 15

Informační plocha
  92X, 4Y 

  Score
  Rychlost
  Combo

*/

class Race{

public:

  Race();
  void start(); 
  void switchLane();
  void reset();
  void turn();
  float gameSpeed; ///< Current game speed (more means faster)
  bool gameOver; ///< State of the game (true - game is over, false - game running)

private:

  void drawCar(int x, int y, bool dir);
  void drawInterface();
  void drawPlayer(int x);
  void drawEnemy(int x, int y);
  void drawMoving(bool draw);
  void addEnemy();

  char buff[20]; ///< Buffer used for parsing
  int gameTurn; ///< Number of turns from begining of the game
  unsigned long gameScore; ///< Player score
  int pivotChance; ///< Used for balancing lane spawns for enemies

  int leftLane[25]; ///< Represents the left lane of the race (0 - empty, 1 - enemy)
  int rightLane[25]; ///< Represents the right lane of the race (0 - empty, 1 - enemy)

  bool playerPos; ///< Lane where player is (false - left, true - right)
  
  
};
/**
 * @brief Adds an enemy to the race. Atemps to change lanes randomly.
 */
void Race::addEnemy(){

  randomSeed(analogRead(0));
  
  if(leftLane[0] != 1 && leftLane[1] != 1 && leftLane[2] != 1 && leftLane[3] != 1 && leftLane[4] != 1 && leftLane[5] != 1 && rightLane[0] != 1 && rightLane[1] != 1 && rightLane[2] != 1 && rightLane[3] != 1 && rightLane[4] != 1 && rightLane[5] != 1){
    
    int RNG = random(1, 10);
    
    if(RNG < pivotChance && rightLane[6] != 1 && rightLane[7] != 1 && rightLane[8] != 1 && rightLane[9] != 1 && rightLane[10] != 1 && rightLane[11] != 1){     
      leftLane[0] = 1; 
      pivotChance --;
    }
    else if (RNG > pivotChance && leftLane[6] != 1 && leftLane[7] != 1 && leftLane[8] != 1 && leftLane[9] != 1 && leftLane[10] != 1 && leftLane[11]) {
      rightLane[0] = 1;
      pivotChance++;
    }
    
  }




}
/*
Vykreslení formule protivníka
Param:
  x - (0,1) levý nebo pravý pruh
  y - (1 - 23) bere všechno ale jen tyhle hodnoty dávají smysl -> vyhreslí se alespoň kus formule

*/
/**
 * @brief Draws enemy on given coordinates
 * 
 * @param x coordinate to draw enemy (0,1)
 * @param y coordinate to draw enemy (1 - 23)
 */
void Race::drawEnemy(int x, int y){

  int _Y = y - 2; //Korekce pro déklu formule
  
  drawCar( 22 + x * 40 , 2 + 6 * _Y , false);
  
}
/*
Vykreslení formule hráče
Param:
  x - (0,1) levý nebo pravý pruh


*/
/**
 * @brief Draws the player on given coordinates
 * 
 * @param x coordinate to draw enemy (0,1)
 */
void Race::drawPlayer(int x){
  
  drawCar( 22 + x*40 , 104 , true);
  
}

/*
Vykreslení formule
Param:
  x,y - souřadnice
  dir - směr jízdy 

*/
/**
 * @brief Draws a car on given coordinates
 * 
 * @param x coordinate to draw enemy
 * @param y coordinate to draw enemy
 * @param direction of the car
 */
void Race::drawCar(int x, int y, bool dir){ 

  int _X = x - 2; //korekce na střed čtverce
  int _Y = y - 2; //korekce na střed čtverce

  

  if(dir){ //Hráč

    EsploraTFT.rect(_X, _Y, 5, 5); //Prostřední
    EsploraTFT.rect(_X, _Y + 6, 5, 5); //Druhý
    EsploraTFT.rect(_X, _Y + 12, 5, 5); //První
    EsploraTFT.rect(_X, _Y - 6, 5, 5); //Čtvrtý
    EsploraTFT.rect(_X, _Y - 12, 5, 5); //Pátý
    EsploraTFT.rect(_X + 6, _Y - 6, 5, 5); //Přední pravé kolo
    EsploraTFT.rect(_X - 6, _Y - 6, 5, 5); //Přední levé kolo
    EsploraTFT.rect(_X + 6, _Y + 12, 5, 5); //Zadní pravé kolo
    EsploraTFT.rect(_X - 6, _Y + 12, 5, 5); //Zadní levé kolo
  
  }
  else{ //Protivník

    if( !(_Y + 12 < 6) && !( _Y + 12 > 116 )){
      
      EsploraTFT.rect(_X, _Y + 12, 5, 5); //První
      
    }
    if( !(_Y + 6 < 6) && !( _Y + 6 > 116 )){
      
      EsploraTFT.rect(_X, _Y + 6, 5, 5); //Druhý
      EsploraTFT.rect(_X + 6, _Y + 6, 5, 5); //Přední pravé kolo
      EsploraTFT.rect(_X - 6, _Y + 6, 5, 5); //Přední levé kolo
      
    }
    
    if( !(_Y < 6) && !( _Y > 116 )){
      
      EsploraTFT.rect(_X, _Y, 5, 5); //Prostřední
      
    }
    
    if( !(_Y - 6 < 6) && !( _Y - 6 > 116 )){
      
      EsploraTFT.rect(_X, _Y - 6, 5, 5); //Čtvrtý
      
    }
    
    if( !(_Y - 12 < 6) && !( _Y - 12 > 116 )){
      
      EsploraTFT.rect(_X, _Y - 12, 5, 5); //Pátý
      EsploraTFT.rect(_X + 6, _Y - 12, 5, 5); //Zadní pravé kolo
      EsploraTFT.rect(_X - 6, _Y - 12, 5, 5); //Zadní levé kolo
          
    }
    
  }

}
/*
Vykreslý hrací plochu


*/
/**
 * @brief Draws the interface for the game
 */
void Race::drawInterface(){
  
  EsploraTFT.rect(4, 4, 80, 117);
  EsploraTFT.setTextSize(2);
  EsploraTFT.text("SCORE",92,4);
  EsploraTFT.setTextSize(1);
  EsploraTFT.text("SPEED",92,50);
  EsploraTFT.text("HIGHSCORE",92,80);

  unsigned long da;
  EEPROM.get(0, da);

  String val = String(da);
  val.toCharArray(buff, 15);
  EsploraTFT.text(buff,92,90);
  
}
/**
 * @brief Draws/deletes moving element of the game
 * 
 * @param draw/delete
 */
void Race::drawMoving(bool draw){

  if(!draw) EsploraTFT.stroke(255,255,255);
  
  for(int i = 0; i < 25; i++){
    
    if(leftLane[i] == 1) drawEnemy(0, i);
    
  }
  for(int i = 0; i < 25; i++){
    
    if(rightLane[i] == 1) drawEnemy(1, i);
    
  }

  String val = String(gameScore);
  val.toCharArray(buff, 15);
  EsploraTFT.text(buff,92,27);

  val = String(gameSpeed);
  val.toCharArray(buff, 5);
  EsploraTFT.text(buff,130,50);

  if(!draw) EsploraTFT.stroke(0,0,0);
  
}
/**
 * @brief Switches player to the other lane
 */
void Race::switchLane(){
  
  EsploraTFT.stroke(255,255,255);
  drawPlayer(playerPos ? 1 : 0);
  EsploraTFT.stroke(0,0,0);
  playerPos = !playerPos;
  drawPlayer(playerPos ? 1 : 0);

}
/**
 * @brief Constructor, sets up default values for the game
 */
Race::Race(){
  
  for(int i = 0; i < 25; i++){
    
   leftLane[i] = 0;
   rightLane[i] = 0;
    
  }

  playerPos = false;
  gameSpeed = 1;
  gameTurn = 0;
  gameScore = 0;
  gameOver = false;
  pivotChance = 5;
  
}
/**
 * @brief Resets the game to the starting state
 */
void Race::reset(){
 
  for(int i = 0; i < 25; i++){
    
   leftLane[i] = 0;
   rightLane[i] = 0;
    
  }

  playerPos = false;
  gameSpeed = 1;
  gameTurn = 0;
  gameScore = 0;
  gameOver = false;
  pivotChance = 5;

  EsploraTFT.background(255,255,255);
  drawInterface();
  drawPlayer(playerPos ? 1 : 0);

}
/**
 * @brief Moves the game one frame foward
 * 
 *  Moves enemies one step foward
 *  Checks if player colided with enemy
 *  Increases player score
 *  Displays GAMEOVER screan if game is over :D
 */
void Race::turn(){

  if(gameOver) {
    EsploraTFT.background(255,255,255);
    EsploraTFT.setTextSize(3);
    EsploraTFT.text("GAMEOVER",10,50);
    EsploraTFT.setTextSize(1);
    unsigned long da;
    EEPROM.get(0, da);
    if(da < gameScore){
    EEPROM.put(0, gameScore);
    }
    delay(50);
    return;
  }
  
  drawMoving(false);

  if(leftLane[24] == 1) gameSpeed += 0.1;
  for(int i = 24; i > 0; i--){
    
    leftLane[i] = leftLane[i - 1];
    if(i >= 15 && !playerPos && leftLane[i] == 1) gameOver = true;
    
  } 
  leftLane[0] = 0;

  if(rightLane[24] == 1) gameSpeed += 0.1;
  for(int i = 24; i > 0; i--){
    
    rightLane[i] = rightLane[i - 1];
    if(i >= 15 && playerPos && rightLane[i] == 1) gameOver = true;
    
  }  
  rightLane[0] = 0;

  addEnemy();

  gameTurn++;
  gameScore += 100 * gameSpeed;
  
  
  drawMoving(true);

}
/**
 * @brief Starts the game
 * 
 *  Draw interface, player and enemies
 */
void Race::start(){
  
  drawInterface();
  drawMoving(true);
  drawPlayer(playerPos ? 1 : 0);

}

Race game;
bool pressed = false;
char buff[50];

/**
 * @brief Sets up the screen and starts the game
 */
void setup(){
  
  EsploraTFT.begin();  
  EsploraTFT.background(255,255,255);
  EsploraTFT.stroke(0,0,0);
  game.start();
  
}

/**
 * @brief Handles button presses and calls corresponding functions
 */
void loop(){

  unsigned long start = millis();

  while(start + 200 / game.gameSpeed > millis()){
    
    if(!Esplora.readButton(SWITCH_DOWN) && !pressed){
      
      game.switchLane();
      pressed = true;
    
    }
    else if(!Esplora.readButton(SWITCH_UP) && !pressed){
      
      game.reset();
      pressed = true;
    
    }
    else if (Esplora.readButton(SWITCH_UP) && Esplora.readButton(SWITCH_DOWN)){
      
      pressed = false;  
    
    }
    
  }

  game.turn();
  
  

}
